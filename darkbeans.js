/*
 FusionCharts JavaScript Library
 Copyright FusionCharts Technologies LLP
 License Information at <http://www.fusioncharts.com/license>
*/
/**
 * This file create a theme file named darkbeans. The cosmetics for the
 * different charts are defined here.
 *
 * More information on theming can be found at
 * https://www.fusioncharts.com/dev/advanced-chart-configurations/theme-manager/creating-new-themes
 */

FusionCharts.register('theme', {
    name: 'darkbeans',
    theme: {
        base: {
            chart: {
                animation:"1",
                animationDuration:"1",
                interactiveAnimationDuration:"2",
                canvasBgAlpha: "0",
                bgColor: "#266f91,#1b324e",
                bgAlpha: "100",
                bgAngle: "45",
                canvasBorderAlpha: "0",
                paletteColors: "#23e4a3, #9b4aff, #209ff8, #2569f2, #d7223b, #fd5308, #fb9902, #fefe33, #67c227, #1ab8cf",
                baseFont: "Nunito Sans Regular",
                outCnvBaseFont:"Nunito Sans Semi-bold",
                showCanvasBase: "1",
                canvasBaseColor: "#4c7891",
                divLineColor: "#3c7fa8",
                divLineThickness: "1",
                divLineDashed: "1",
                divLineDashLen: "5",
                divLineDashGap: "2",
                divLineAlpha: "100",
                tooltipBgColor: "#FFFFFF",
                tooltipBorderColor: "#375674",
                tooltipColor: "#375674",
                toolTipFontSize: "12",
                tooltipPadding: "8",
                tooltipBorderRadius: "5",
                tooltipBorderAlpha:"0",
                toolTipBgAlpha: "100",
                showAlternateHGridColor: "1",
                alternateHGridColor: "#1e4762",
                showXAxisLine: "1",
                xAxisLineColor: "#559ac5",
                xAxisLineThickness: "1",
                usePlotGradientColor: "0",
                use3dlighting:"1",
                showShadow: "0",
                showPlotBorder: "0",
                showValues: "0",
                baseFontSize: "14",
                captionFontSize: "24",
                captionFont:"Nunito Sans Light",
                captionFontColor: "#e5f4fd",
                captionFontBold: "0",
                subCaptionFontSize: "16",
                subCaptionFont:"Nunito Sans Regular",
                subCaptionFontColor: "#6ba1bf",
                alignCaptionWithCanvas:"1",
                labelFontSize: "14",
                labelFont:"Nunito Sans Semi-bold",
                labelFontColor: "#7fd0fa",
                xAxisNameFontSize: "16",
                xAxisNameFontColor: "#aed8f0",
                yAxisNameFont:"Nunito Sans Light",
                yAxisNameFontSize: "16",
                yAxisNameFontColor: "#aed8f0",
                syAxisNameFont:"Nunito Sans Light",
                syAxisNameFontSize: "16",
                syAxisNameFontColor: "#aed8f0",
                legendItemFont:"Nunito Sans Regular",
                legendFontSize: "14",
                legendFontColor: "#aed8f0",
                legendBgColor: "#000000",
                legendBgAlpha: "40",
                legendBorderAlpha: "0",
                outCnvBaseFontColor: "#7fd0fa",
                outCnvBaseFontSize: "12",
                chartTopMargin:"25",
                chartBottomMargin: "20",
                chartLeftMargin: "20",
                captionPadding: "30",
                canvasPadding: "10",
                captionPadding: "30",
                xAxisNamePadding: "30",
                yAxisNamePadding: "30",
                labelPadding: "10",
                anchorBgColor: "#113344",
                anchorHoverEffect: "0",
                anchorRadius: "5",
                anchorBorderThickness: "1",
                flatScrollBars: "1",
                scrollShowButtons: "0",
                scrollColor: "#25627f",
                scrollHeight:"15",
                showHoverEffect:"1",
                plotHoverEffect:"1",
                valuePadding:"10",
            },
            trendlines: [{
                line: [{
                    color: "#FFFFFF",
                    alpha: "100",
                    thickness: '2',
                    dashed: "1",
                    valueOnRight: "0"
                }]
            }]
        },

        column2d: {
            chart: {
                plotSpacePercent: "60"
            }            
        },

        column3d: {
            chart: {
            	plotSpacePercent: "60"
            }
        },
         bar2d: {
            chart: {
                plotSpacePercent: "40"
            }
        },

        bar3d: {
            chart: {
                plotSpacePercent: "40"
            }
        },
 pie2d: {
            chart: {
                showPercentInToolTip: '0',
                animateClockwise: "0",
                labelFontColor: "#7fd0fa",
                startingAngle: "270",
                pieRadius:"150",
                showPercentValues: "1",
                enableSmartLabels: '1',
                enableSlicing:"1",
                enableMultiSlicing:"1",
                smartLineColor:"#7fd0fa",
                showLegend: "1",
                use3dlighting:"0",
            }
        },
        pie3d: {
            chart: {
                showPercentInToolTip: '0',
                pieRadius:"200",
                animateClockwise: "0",
                labelFontColor: "#7fd0fa",
                startingAngle: "270",
                showPercentValues: "1",
                enableSmartLabels: '1',
                enableSlicing:"1",
                enableMultiSlicing:"1",
                smartLineColor:"#7fd0fa",
                pieSliceDepth:"20",
                showLegend: "1"

            }
        },
        doughnut2d: {
            chart: {
                showPercentInToolTip:'0',
                animateClockwise: "0",
                labelFontColor: "#7fd0fa",
                centerLabelColor:"#7fd0fa",
                centerLabelFont:"Nunito Sans Semi-bold",
                startingAngle: "270",
                showPercentValues: "1",
                enableSmartLabels: '1',
                smartLineColor:"#7fd0fa",
                showLegend: "1",
                use3dlighting:"0",
                pieRadius :"150"

            }
        },
        doughnut3d: {
            chart: {
                showPercentInToolTip: '0',
                animateClockwise: "0",
                use3DLighting: "1",
                labelFontColor: "#7fd0fa",
                centerLabelColor:"#7fd0fa",
                centerLabelFont:"Nunito Sans Semi-bold",
                startingAngle: "270",
                showPlotBorder: "0",
                showPercentValues: "1",
                enableSmartLabels: '1',
                smartLineColor:"#7fd0fa",
                showLegend: "1",
                pieRadius:"200"

            }
        },
        pareto2d:{
            chart:{
                lineColor:"#FFFFFF",
                lineThickness:"3"
            }
        },
        pareto3d:{
            chart:{
                lineColor:"#FFFFFF",
                lineThickness:"3",
                plotSpacePercent: "40"
            }
        },
        mscolumn2d: {
        	chart: {
        		plotSpacePercent: "40",
                showShadow: "1"
        	}
        },

        mscolumn3d: {
        	chart: {
        		plotSpacePercent: "40"
        	}
        },

        msbar2d: {
        	chart: {
        		plotSpacePercent: "30",
                showShadow: "1"
        	}
        },

        msbar3d: {
        	chart: {
        		plotSpacePercent: "20"
        	}
        },

        area2d: {
        	chart: {
        		plotFillAlpha: "80",
        		showAnchors: "0"
        	}
        },

        msarea: {
        	chart: {
        		plotFillAlpha: "80",
        		showAnchors: "0"
        	}
        },

        line: {
        	chart: {
                showCanvasBorder: "1",
                canvasBorderThickness: "1",
                canvasBorderAlpha: "30",
                canvasBorderColor: "#595f90"
        	}
        },

        msline: {
            chart: {
                showCanvasBorder: "0",
                canvasBorderThickness: "0",
                canvasBorderAlpha: "0",
                showXAxisLine:"0",
                canvasBorderColor: "#3c7fa8"
            }
        },
        zoomline:{
            chart:{
                showAlternateHGridColor:"0",
                pixelsPerPoint: "0",
                pixelsPerLabel: "30",
                lineThickness: "2",
                compactdatamode : "1",
                dataseparator : "|",
                forceAxisLimits : "1",
                pinPaneBgColor:"1b324e",
                pinPaneBgAlpha:"30",
                zoomPaneBgAlpha:"60",
                zoomPaneBgColor:"1b324e",
                toolbarButtonColor:"1b324e",
                borderColor:"#6ba1bf",
                crossLineColor:"#3c7fa8",
                crossLineThickness:"2"

            }
        },
        zoomlinedy:{
            chart:{
                showAlternateHGridColor:"0",
                pixelsPerPoint: "0",
                pixelsPerLabel: "30",
                lineThickness: "2",
                compactdatamode : "1",
                dataseparator : "|",
                forceAxisLimits : "1",
                pinPaneBgColor:"1b324e",
                pinPaneBgAlpha:"30",
                zoomPaneBgAlpha:"60",
                zoomPaneBgColor:"1b324e",
                toolbarButtonColor:"1b324e",
                borderColor:"#6ba1bf"
               
            }
        },
        msstackedcolumn2d: {
        	chart: {
        		plotSpacePercent: "50",
                showShadow: "1"
        	}
        },
        msstackedcolumn2dlinedy:{
            chart: {
                plotSpacePercent: "50",
                showShadow: "1",
                lineThickness:"2"
            }
        },
        stackedcolumn2d: {
        	chart: {
        		plotSpacePercent: "60",
                showShadow: "1"
        	}
        },

        stackedcolumn3d: {
        	chart: {
        		plotSpacePercent: "60"
        	}
        },

        stackedbar2d: {
        	chart: {
        		plotSpacePercent: "40",
                showShadow: "1"
        	}
        },

        stackedbar3d: {
        	chart: {
        		plotSpacePercent: "40"
        	}
        },

        stackedarea2d: {
        	chart: {
        		plotFillAlpha: "80",
        		showAnchors: "0"
        	}
        },
        mscombi2d: {
        	chart: {
        		showAnchors: "1"
        	}
        },
        mscombidy2d:{
            chart:{
                showAnchors: "0"
            }
        },
        mscombi3d:{
           chart: {
                showAnchors: "0",
                plotSpacePercent: "50"
            } 
        },
        mscolumnline3d:{
            chart:{
                plotSpacePercent: "50"
            }

        },
        mscolumn3dlinedy:{
             chart:{
                plotSpacePercent: "50"
            }
        },
        stackedcolumn2dline:{
            chart:{
                plotSpacePercent: "70"
            }
        },
        stackedcolumn3dline:{
            chart:{
                plotSpacePercent: "80"
            }
        },
        stackedcolumn3dlinedy:{
            chart:{
                plotSpacePercent: "80"
            }
        },       
        marimekko:{
            chart:{
                showSum:"0",
                showxaxispercentvalues: "1",
                usePercentDistribution:"1",
                valueFontcolor:"#7fd0fa",
                valueBgAlpha:"60",
                valueBgColor:"#09131e",
                valueBorderAlpha:"0",
                valueBorderThickness:"0",
                valueBorderPadding:"10",
                showAlternateHGridColor:"0"
            }
        },
        bubble: {
            chart: {
                showYAxisLine: "1",
                yAxisLineColor: "#559ac5",
                yAxisLineThickness: "1",
                valueFontSize: "12",
                bubbleScale: "1.5",
                plotFillAlpha: "90",
                plotBorderAlpha: "0",
                plotFillHoverColor: "#468cad",
                plotBorderHoverAlpha: "0",
                valueFontColor:"#000000"
            },
            trendlines: [
                {
                    
                    line: [
                        {
                            color: "#164157",
                            alpha: "0"
                        }, 
                        {
                            color: "#164157",
                            alpha: "15"
                        }, 
                        {
                            color: "#164157",
                            alpha: "60"
                        }
                    ]
                }
            ]
        },
        scatter:{
            chart: {
                showYAxisLine: "0",
                plotFillAlpha: "90",
                plotBorderAlpha: "0",
                showAlternateHGridColor:"0"
            }
      
        },     
        scrollColumn2d:{
            chart:{
                numVisiblePlot:"15",
                showAlternateHGridColor:"1"
            }
        },
        scrollline2d:{
            chart:{
                numVisiblePlot:"15",
                labelDisplay:"rotate",
                showAlternateHGridColor:"1"
            }
        },
        scrollarea2d:{
            chart:{
                plotFillAlpha: "80",
                numVisiblePlot:"15",
                labelDisplay:"rotate",
                showAlternateHGridColor:"1",
                showAnchors:"0"
            }
        },
        scrollstackedcolumn2d:{
            chart:{
                numVisiblePlot:"15",
                showAlternateHGridColor:"1",
                labelDisplay:"rotate"
            }
        },
        scrollcombi2d:{
            chart:{
                plotFillAlpha: "80",
                numVisiblePlot:"14",
                labelDisplay:"rotate",
                showAlternateHGridColor:"1",
                showAnchors:"0"
            }
        },
         angulargauge: {
                chart:{
                        showValue: "0",
                        gaugeOuterRadius:"300",
                        gaugeInnerRadius:"0",
                        showShadow:"1",
                        placeTicksInside:"0",
                        majorTMHeight:"13",
                        minorTMHeight:"6",
                        tickValueDistance:"10",
                        pivotFillColor: "#5599CC",
                        pivotFillAlpha: "100",
                        showPivotBorder:"1",
                        pivotBorderThickness:"7",
                        pivotBorderColor:"#000000",
                        pivotRadius:"15",
                        subCaptionFontColor:"#e5f4fd",
                        baseFontColor:"#e5f4fd",
                        gaugeFillMix: "{dark-80},{light-60},{dark-40}",
                },
                                dials: {
                                        dial: [{
                                                baseWidth: "25",
                                                rearExtension: "30",
                                                bgColor: "#000000",
                                                bgAlpha: "100",
                                                borderColor: "#000000",
                                                bgHoverAlpha: "20",
                                                radius:"290"
                                        }]
                                }
        },
        
        thermometer:{
            chart:{
                showTickMarks:"1",
                ticksOnRight:"1",
                majorTMColor:"#7fd0fa",
                gaugeFillColor:"#7fd0fa",
                showValues:"1",
                valueFontcolor:"7#fd0fa",
                valuePadding:"20"
            }
        },
        cylinder:{
            chart:{
                showTickMarks:"1",
                ticksOnRight:"1",
                majorTMColor:"#7fd0fa",
                cylFillColor:"#23e4a3",
                showValues:"1",
                valueFontcolor:"#7fd0fa",
                valuePadding:"20",
                cylRadius:"100",
                cylHeight:"300",
                cylGlassColor:"#1f5b77",
                showBorder:"1",
                borderThickness:"3",
                borderColor:"#1f5b77"

            }
        },
        bulb:{
            chart:{
                showValue:"1",
                is3d:"1",
                placeValuesInside:"1",
                useColorNameAsValue:"1",
                valueFontSize: "18",
                is3donHover:"1",
                valueFontColor:"#000000"

            }
        },   
        vled:{
                chart:{
                      upperLimit:"120",
                      lowerLimit:"0",
                      showGaugeBorder:"0",
                      ledGap:"4",
                      ledSize:"4",
                      useSameFillColor:"1",
                      useSameFillBgColor:"1",
                      valueFontColor:"#7fd0fa",
                      ticksOnRight:"0",
                      showTickMarks:"1",
                      showTickValues:"1",
                      majorTMColor:"#7fd0fa",
                      baseFontColor:"7#fd0fa"

                }
        },
        hled:{
                chart:{
                       upperLimit:"100",
                      lowerLimit:"0",
                      showGaugeBorder:"0",
                      borderAlpha:"0",
                      useSameFillColor:"0",
                      useSameFillBgColor:"0",
                      ledGap:"4",
                      ledSize:"4",
                      valueFontColor:"7fd0fa",
                      ticksBelowGauge:"1",
                      gaugeBorderAlpha:"0",
                      showTickMarks:"1",
                      showTickValues:"1",
                      majorTMColor:"#7fd0fa",
                      baseFontColor:"7#fd0fa"
                }
        },
        realtimearea:{
            chart:{
                showAnchors:"0",
                plotFillAlpha: "80",
                canvasPadding: "15"
            }
        },
        realtimestackedarea:{
            chart:{
                showAnchors:"0",
                plotFillAlpha: "80",
                canvasPadding: "15"
            }
        },
        sparkline:{
            chart:{
               
                 chartTopMargin: "90",
                 showBorder: "0",
                 lineColor:"#FFFFFF",
                 captionOnTop:"0",
                 captionPosition:"middle",
                 captionFontSize:"28",
                 captionFontColor:"#FFFFFF",
                 captionFontBold:"1",
                 lineThickness:"1",
                 showShadow:"0",
                 showXAxisLine:"0",
                 valueFontSize:"14"
            }
        },
        sparkcolumn:{
            chart:{
                plotFillColor: "#0075c2",
                highColor: "#1aaf5d",
                lowColor: "#8e0000"
            }
        },
        hbullet: {
            chart: {
            
                
                plotFillColor: "#23e4a3",
                targetColor: "#23e483",
                showHoverEffect: "1",
                showBorder: "0",
                showShadow: "0",
                valuePadding: "7",
                ticksBelowGraph: "1",
                showTickValues:"1",
                majorTMColor:"#6ba1bf",
                adjustTM: "1",
                placeTicksInside: "0",
                placeValuesInside: "0",
                captionFontSize: "16",
                captionPadding:"15",
                valueFontColor:"#6ba1bf"
            }
        },
        vbullet: {
            chart: {      
                plotFillColor: "#23e4a3",
                targetColor: "#23e483",
                showBorder: "0",
                valuePadding: "7",
                ticksBelowGraph: "1",
                showTickValues:"1",
                majorTMColor:"#6ba1bf",
                adjustTM: "1",
                placeTicksInside: "0",
                placeValuesInside: "0",
                captionFontSize: "14",
                valueFontColor:"#6ba1bf"             

            }
        },
         pyramid:{
            chart:{
                smartLineColor:"#65abcc"
            }
        },
        funnel:{
            chart:{
                smartLineColor:"#65abcc",
                isHollow: "1",
                labelDistance: "15"
            }
        },
        gantt:{
            chart:{
                bgAlpha:"90",
                canvasBgColor:"#062136",
                canvasBgAlpha:"90",
                showtasknames:"1",
                chartLeftMargin:"50",
                chartRightMargin:"50",
                chartTopMargin:"50",
                chartBottomMargin:"50",
                dateformat:"dd/mm/yyyy",
                gridBorderColor:"#014060",
                ganttLineColor:"#014060",
                extendCategoryBg:"1",
                showGanttPaneHoverBand:"1",
                taskBarRoundRadius:"2",
                showGanttPaneVerticalHoverBand:"1",
                showGanttPaneHorizontalHoverBand:"1",
                showConnectorHoverEffect:"1",
                showTaskHoverEffect:"1",
                showLegend:"1",
                scrollColor:"#062136",
                scrollHeight:"1",
            },
             categories:[

                 {
                    align:"center",
                    bgAlpha:"80",
                    bgColor:"#013044",
                    verticalPadding:"0",
                    isBold:"1",
                    fontColor:"#89A5B0",
                    fontSize:"20"
                 },
                 {
                    bgColor:"#004662",
                    bgAlpha:"80",
                    isBold:"1"
                 }
             ],
             processes:{
                    bgColor:"#005075",
                    bgAlpha:"80",
                    positioningrid:"right",
                    width:"80",
                    headerBgColor:"#03252F",
                    headerBgAlpha:"90",
                    headerFontColor:"#89A5B0",
                    headerFont:"Nunito Sans Semi-bold",
                    headerIsBold:"1",
                    headerFontSize:"20"
             },
             datatable:{
                showprocessname:"1",
                bgColor:"#015178",
                bgAlpha:"100",
                headerBgColor:"#002639",
                headerBgAlpha:"10",
                headerFontColor:"#89A5B0",
                headerFont:"Nunito Sans Semi-bold",
                headerIsBold:"1",
                headerFontSize:"20",
                datacolumn:[
                    {
                        width:"150"
                    }
                ]
             },
             tasks:{
                 task:[
                {
                  color:"#16CD8C",
                  borderColor:"#126E5E",
                  font:"Nunito Sans Bold",
                  fontColor:"#098395"
                }

                ]

            },
            connectors:[
                {
                    color:"#22D498"
                }

            ],
            milestones:{
                milestone:[
                {
                    radius: "10",
                    shape: "Star",
                    numsides: "5",
                    color:"#FFFFFF"
                }
                ]               
            }
        },
        splinearea:{
            chart:{
                showAnchors:"0"
            }
        },
        mssplinearea:{
            chart:{
               showAnchors:"0" 
            }
        },  
        errorbar2d:{
            chart:{
                errorBarColor:"#1ab8cf",
                errorBarThickness:"2"
            }
        },
        errorline:{
            chart:{
                errorBarColor:"#1ab8cf",
                errorBarThickness:"2",
                halferrorbar: "0",
                halfverticalerrorbar: "0"
            }
        },
        errorscatter:{
            chart:{
                errorBarColor:"#1ab8cf",
                errorBarThickness:"2",

            }
        },
        inverseMSArea:{
            chart:{
                showAnchors:"0"
            }
        }, 
        dragarea:{
            chart:{
                showAnchors:"0"
            }
        },
        treemap:{
            chart:{
                showChildLabels:"0",
                legendBgAlpha: "0",
                legendScaleLineThickness: "0", 
                showBorder:"1",
                borderColor:"#266e8f",
                borderAlpha:"40",
                legendPadding: "50",
                horizontalPadding: ".5",
                verticalPadding: ".5",
                hoveronallsiblingleaves: "1",
                plotFillHoverAlpha:"0",
                legendCaptionFontBold:"0",
                legendShadow: "1",
                legendBorderThickness: "0",
                legendScaleLineAlpha: "0",
                legendCaptionFontSize: "18",
                legendpadding: "0",
                plotBorderColor: "#266e8f",
                plotBorderThickness: ".5",
                animation: "1",
                labelGlow: "0",
                showLegend: "1",
                legendItemFontSize: "15",
                legendItemFontBold: "0",
                legendPointerWidth: "8",
                algorithm: "squarified",
                showParent:"1",
                parentLabelLineHeight:"30"
                
            },
            data:[{
                fillColor:"#0e2031",
                data:[{
                   fillColor:"#18334d" 
                }]
            }]
        },
        radar:{
            chart:{
                radarFillAlpha:"0",
                plotFillAlpha:"70",
                showAnchors:"0",
                showBorder:"0",
                radarBorderColor:"#FFFFFF",
                radarSpikeColor:"#FFFFFF",
                radarSpikeThickness:"1",
                divLineColor:"#FFFFFF",
                legendPosition:"BOTTOM",
                drawCustomLegendIcon:"1",
                legendIconSides:"5",
                legendIconScale:"1",
                legendBorderThickness:"3",
                captionPadding:"70",
                labelPadding:"20"
            }
        },
         heatmap:{
            chart:{
                showBorder:"1",
                plotFillAngle:"45",
                showPlotBorder: "1",
                plotBorderAlpha: "100",
                plotBorderThickness:"1",
                legendIconScale:"1",
                legendItemFontSize: "15",
                legendBgAlpha: "0",               
                xAxisLineColor:"#000000",
                yAxisLineColor:"#000000"
            }
          
        },
        boxandwhisker2d:{
            chart:{
                showsd: "1",
                showmd: "1",
                showqd: "1",
                showDetailedLegend:"0",
                legendShadow:"0",
                medianColor:"#cc2282",
                medianThickness:"1",
                lowerQuartileColor:"#cc2282",
                lowerWhiskerColor:"#cc2282",
                upperWhiskerColor:"#1ab8cf",
                SDIconColor:"#209ff8",
                QDIconColor:"#fefe33",
                MDIconColor:"#fb9902",
                outlierIconColor:"#ffffff"
            }
        },
        candlestick:{
            chart:{
                showVolumeChart:"1",
                volumeHeightPercent:"40",
                showZeroPlaneValue:"0",
                showAlternateHGridColor:"1",
                bearFillColor:"#23e4a3",
                bearBorderColor:"#23e4a3",
                bullFillColor:"#9b4aff",
                showLegend:"0",
                divLineThickness:".5",
                alternateHGridAlpha:"90",
                showHoverEffect:"1",
                plotHoverEffect:"1",
                plotFillHoverColor:"#D67802",
                plotFillHoverAlpha:"90"
                
            },
            trendset:[
            {
                thickness:"0"
            }]
        },

        dragnode:{
            chart:{
                showXAxisLine:"0",
                viewMode:"0",
                showRestoreBtn:"0",
                valueFontcolor:"#000000",
                valueFontSize:"11",
                valueFontBold:"1",
                valueBorderPadding:"10"
            },
            connectors:{
                color:"#23e4a3",
                stdThickness:".5",
                strength:".5"
            }

        },
        multiAxisLine:{
            chart:{
                legendPadding:"50"
            },
            axis:{
                divLineColor: "#3c7fa8"
            }
        },
         multilevelpie:{
            chart:{
                baseFontColor:"#FFFFFF",
                showBorder : "0",
                showCanvasBorder: "0",
                pieFillAlpha: "80",
                pieBorderThickness: "1",
                hoverFillColor: "#788e98",
                pieBorderColor: "#FFFFFF",
                useHoverColor: "1",
                plotFillHoverAlpha:"1",
                showValuesInTooltip: "1",
                showPercentInTooltip: "0",
                shadowColor:"#000000",
                shadowAlpha:"100",
                plotTooltext: "$label, $$valueK, $percentValue"
            }
        },
         selectscatter:{
            chart:{
                showYAxisLine: "0",
                plotFillAlpha: "90",
                plotBorderAlpha: "0"
            }
        },
         waterfall2d:{
            chart:{
                connectordashed:"1",
                showConnectors:"1",
                connectorColor:"#8ea8b3",
                connectorThickness:"2",
                plotBorderAlpha:"0"
            }
        },
         kagi:{
            chart:{
                anchorRadius:"4",
                rallyColor:"#23e4a3",
                declineColor:"cc2282"
            }
        },
        geo: {
            chart: {
                showLabels: "0",
                fillColor: "#0075C2",
                entityFillHoverColor:"#6799b0",
                showBorder:"1",
                borderColor:"#ffffff",      
                borderThickness: ".8",
                showLabels:"1",
                borderAlpha: "80",
                baseFontColor:"#000000",
                baseFontSize:"14",
                baseFont:"Nunito Sans Bold",
                entityFillhoverAlpha: "80",
                connectorColor: "#cccccc",
                connectorThickness: "1.5",
                markerFillHoverAlpha: "90",
                legendBgAlpha:"0",
                legendScaleLineAlpha: "0",
                legendScaleLineColor:"#0075C2",
                legendScaleLineAlpha:"80",
                legendScaleLineThickness:"1"
            }
        }
    }
});